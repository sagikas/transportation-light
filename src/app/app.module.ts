import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FiltersComponent } from './filters/filters.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { LabelModule } from '@progress/kendo-angular-label';
import { FormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DataGridComponent } from './data-grid/data-grid.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { RTL } from '@progress/kendo-angular-l10n';
import { MainComponent } from './main/main.component';
import { ReportsComponent } from './reports/reports.component';
import { ReportsFiltersComponent } from './reports-filters/reports-filters.component';
import { InprocessFiltersComponent } from './inprocess-filters/inprocess-filters.component';
import { InprocessComponent } from './inprocess/inprocess.component';
import { LinesFiltersComponent } from './lines-filters/lines-filters.component';
import { LinesComponent } from './lines/lines.component';


@NgModule({
  declarations: [
    AppComponent,
    FiltersComponent,
    DataGridComponent,
    MainComponent,
    ReportsComponent,
    ReportsFiltersComponent,
    InprocessFiltersComponent,
    InprocessComponent,
    LinesFiltersComponent,
    LinesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputsModule,
    ButtonsModule,
    BrowserAnimationsModule,
    DropDownsModule,
    LabelModule,
    FormsModule,
    GridModule
  ],
  // providers:    [],
  providers:    [{ provide: RTL, useValue: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
