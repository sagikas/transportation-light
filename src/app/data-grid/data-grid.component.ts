import { Component, OnInit } from '@angular/core';
import { products } from 'src/assets/mock/products';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss']
})
export class DataGridComponent implements OnInit {

  constructor() {
    this.loadItems();
  }
  public gridView: GridDataResult;
  public pageSize = 10;
  public skip = 0;
  private data: Object[];
  private items : any[] = products;
  ngOnInit(): void {
    console.log(this.items)
  }


  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
}

private loadItems(): void {
  console.log("loadItems() = ")
  console.log(this.items)

    this.gridView = {
        data: this.items.slice(this.skip, this.skip + this.pageSize),
        total: this.items.length
    };
}
}
