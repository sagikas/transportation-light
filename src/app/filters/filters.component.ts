import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  public allowCustom: boolean = true;
  public listItems: Array<string> = ['ראשון', 'שני', 'שלישי', 'רביעי'];

constructor() { }
  ngOnInit(): void {
  }

}
