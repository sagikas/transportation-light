import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InprocessFiltersComponent } from './inprocess-filters.component';

describe('InprocessFiltersComponent', () => {
  let component: InprocessFiltersComponent;
  let fixture: ComponentFixture<InprocessFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InprocessFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InprocessFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
