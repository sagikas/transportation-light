import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinesFiltersComponent } from './lines-filters.component';

describe('LinesFiltersComponent', () => {
  let component: LinesFiltersComponent;
  let fixture: ComponentFixture<LinesFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinesFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinesFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
