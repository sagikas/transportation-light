import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lines-filters',
  templateUrl: './lines-filters.component.html',
  styleUrls: ['./lines-filters.component.scss']
})
export class LinesFiltersComponent implements OnInit {
  public allowCustom: boolean = true;
  public listItems: Array<string> = ['ראשון', 'שני', 'שלישי', 'רביעי'];

  selectedStatus = null;
  statusOptions = [
    { id: -1, name: 'מאושר' },
    { id: 2, name: 'ממונה' },
    { id: 8, name: 'מפות חירום' },
    { id: 1, name: 'מפעיל' },
    { id: 5, name: 'מפת מכרז' }];
  constructor() { }

  ngOnInit(): void {
    this.selectedStatus = 2;
  }

}

